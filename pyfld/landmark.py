import cv2
import mxnet as mx
import numpy as np

from typing import Optional


class LandmarkDetector:
    def __init__(
        self,
        prefix: str,
        epoch: int,
        im_size: Optional[int] = None,
    ):
        if im_size is None:
            im_size = 192
        image_size = (im_size, im_size)
        self.M = np.array([[0.57142857, 0.0, 32.0], [0.0, 0.57142857, 32.0]])
        self.IM = np.array([[[1.75, -0.0, -56.0], [-0.0, 1.75, -56.0]]])
        ctx = mx.cpu()
        sym, arg_params, aux_params = mx.model.load_checkpoint(prefix, epoch)
        all_layers = sym.get_internals()
        sym = all_layers["fc1_output"]
        self.image_size = image_size
        model = mx.mod.Module(symbol=sym, context=ctx, label_names=None)
        model.bind(
            for_training=False,
            data_shapes=[("data", (1, 3, image_size[0], image_size[1]))],
        )
        model.set_params(arg_params, aux_params)
        self.model = model
        self.image_size = image_size

    def transform_points_2d(self, pts: np.ndarray) -> np.ndarray:
        new_pts = np.zeros(shape=pts.shape, dtype=np.float32)
        for i in range(pts.shape[0]):
            pt = pts[i]
            new_pt = np.array([pt[0], pt[1], 1.0], dtype=np.float32)
            new_pt = np.dot(self.M, new_pt)
            new_pts[i] = new_pt[0:2]
        return new_pts

    def get(self, img: np.ndarray) -> np.ndarray:
        input_blob = np.zeros((1, 3) + self.image_size, dtype=np.float32)
        processed_image = cv2.warpAffine(img, self.M, self.image_size, borderValue=0.0)
        processed_image = cv2.cvtColor(processed_image, cv2.COLOR_BGR2RGB)
        processed_image = np.transpose(processed_image, (2, 0, 1))  # 3*112*112, RGB

        input_blob[0] = processed_image
        data = mx.nd.array(input_blob)
        db = mx.io.DataBatch(data=(data,))
        self.model.forward(db, is_train=False)
        pred = self.model.get_outputs()[-1].asnumpy()[0]
        pred = pred.reshape((-1, 2))
        pred[:, 0:2] += 1
        pred[:, 0:2] *= self.image_size[0] // 2
        pred = self.transform_points_2d(pred)

        return pred
