import datetime
import numpy as np

from pyfld.landmark import LandmarkDetector


def main():
    detector = LandmarkDetector(prefix='2d106det', epoch=0)
    frame = np.load('landmark_input.npy')
    _start = datetime.datetime.utcnow()
    res = detector.get(frame)
    _end = datetime.datetime.utcnow()
    print('Inference time: ', (_end - _start).total_seconds())
    return res


if __name__ == "__main__":
    start = datetime.datetime.utcnow()
    main()
    end = datetime.datetime.utcnow()
    print('Run time: ', (end - start).total_seconds())
